<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVideosRealtionCaches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos_users_relations_cache', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['seen', 'like', 'bookmark', 'full_seen']);
            $table->integer('time_period_in_day');
            $table->integer('count');
            $table->integer('created_at');
            $table->integer('updated_at');
            $table->index(['time_period_in_day', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
