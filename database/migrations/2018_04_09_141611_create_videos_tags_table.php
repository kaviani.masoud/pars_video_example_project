<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('video_id');
            // $table->foreign('videos_id')->references('id')->on('videoes');
            $table->integer('tag_id');
            // $table->foreign('tags_id')->references('id')->on('tags');
            $table->integer('created_at');
            $table->integer('updated_at');
            $table->index(['video_id', 'tag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos_tags');
    }
}
