<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VideosUsersRelationsCacheSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = time();
        DB::table('videos_users_relations_cache')->insert([
            'type' => 'seen',
            'time_period_in_day' => '0',
            'count' => '0',
            'created_at' => $time,
            'updated_at' => $time
        ]);
    }
}
