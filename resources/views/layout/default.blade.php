<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $title ?? '' }} | ParsVideo | آپلود ویدیو</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    
    <!-- Custom styles for this template -->
    <link href="{{URL::asset('assets/css/clean-blog.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/vendor/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/vendor/MDBootstrapPersianDateTimePicker/Content/MdBootstrapPersianDateTimePicker/jquery.Bootstrap-PersianDateTimePicker.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/vendor/Iransans/css/fontiran.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet">
    <meta name="fontiran.com:license" content="LZC7A">
    @yield('canonical')

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="{{url('/')}}">ParsVideo(آپلود ویدیو)</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          منو
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="{{route('user_upload_video')}}">آپلود ویدیو</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('user_youtube')}}">آپلود از یوتیوب</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('user_video_list')}}">ویدیوهای من</a>
            </li>
            <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">ورود</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">ثبت نام</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        خروج
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Header -->

    <!-- Post Content -->
            @yield('content')

    <hr>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <!--<ul class="list-inline text-center">
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
            </ul>-->
            <p class="copyright text-muted">ساخته شده با
              <i class="fa fa-heart"></i>
              جهت اشتراک دانش
              <br />
              <a href="{{ url('/') }}">پارس ویدیو</a>  | تصاویر شما...</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{URL::asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/MDBootstrapPersianDateTimePicker/Scripts/MdBootstrapPersianDateTimePicker/jalaali.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/MDBootstrapPersianDateTimePicker/Scripts/MdBootstrapPersianDateTimePicker/jquery.Bootstrap-PersianDateTimePicker.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{URL::asset('assets/js/clean-blog.min.js')}}"></script>
    @yield('scripts')
  </body>

</html>
