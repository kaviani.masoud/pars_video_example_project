<?php
    $message_and_class = Message::consume();
?>
@if ($message_and_class)
    <div class="alert alert-{{ $message_and_class['class'] }}">
        {{ $message_and_class['message'] }}
    </div>
@endif