<table class="table">
    <tr>
        <th>
            عنوان
        </th>
        <th>
            وضعیت
        </th>
        <th>
            
        </th>
    </tr>
    @foreach ($videos as $video)
        <tr class="post-preview">
        <td>
            <a href="{{ route('single_video', ['id' => $video->id, 'url_text_link' => URLHelper::generate_title_for_url($video->title)])  }}">
                <i class="fas fa-video"></i> {{$video->title}}
            </a>
        </td>
        <td>
            {{ Type::humanReadablesResolve('video_statuses', $video->status) }}
        </td>
        <td>
            <!--<a href="{{ route('user_video_edit', ['id' => $video->id]) }}"><i class="fa fa-edit"></i></a>-->
            <a href="{{ route('user_video_delete', ['id' => $video->id]) }}"><i class="fa fa-times"></i></a>
        </td>
           
        </tr>
    @endforeach
</table>