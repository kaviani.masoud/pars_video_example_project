@extends('layout.default')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
<form method="POST" action="{{route('user_youtube_download')}}" enctype="multipart/form-data">
@csrf
    <div class="control-group">
            <div class="form-group floating-label-form-group controls">
                <lable for="source_link">
                    آدرس کامل Youtube
                    <input type="text" class="form-control"  name="address" id="address" placeholder="عنوان" required data-validation-required-message="یک عنوان مناسب برای ویدیو بگذارید"/>
                </lable>
                <p class="help-block text-danger">
                @if ($errors->has('address'))
                        <strong>{{ $errors->first('address') }}</strong>
                @endif
            </p>
            </div>
    </div>
    <div class="control-group">
        <div class="form-group floating-label-form-group controls">
            <lable for="source_link">
                عنوان ویدیو(یک عنوان مناسب برای ویدیو بگزارید)
                <input type="text" class="form-control"  name="title" id="title" placeholder="عنوان" required data-validation-required-message="یک عنوان مناسب برای ویدیو بگذارید"/>
            </lable>
            <p class="help-block text-danger">
                @if ($errors->has('title'))
                        <strong>{{ $errors->first('title') }}</strong>
                @endif
            </p>
        </div>
    </div>
    <div class="control-group">
        <div class="form-group floating-label-form-group controls">
            <lable for="text">
                توضیحات
                <textarea name="description" id="descriptoin" class="form-control" placeholder="توضیحات"></textarea>
            </lable>
            <p class="help-block text-danger"></p>
            </div>
    </div>
    <div class="control-group">
        <div class="form-group floating-label-form-group controls">
            <lable for="source_name">
                           تگ‌ها(با خط تیره جدا شود)
                <input type="text" class="form-control"  name="tags" id="tags" placeholder="تگ‌ها" />
            </lable>
            <p class="help-block text-danger"></p>
            </div>
    </div>
    <div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" id="sendMessageButton">ثبت</button>
        </div>
    </div>
</form>

            </div>
        </div>
    </div>
</div>
@stop
