@extends('layout.default')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('layout.message')
            @include('user.video.partials.get_list_table')
        </div>
    </div>
</div>
@stop
