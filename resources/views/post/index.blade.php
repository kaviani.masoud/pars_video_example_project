@extends('layout.default')
@section('canonical')
<link rel="canonical" href="{{ route('single_post', ['id' => $topic->id, 'url_text_link' => $topic->url_text_link]) }}" />
@stop
@section('content')

  <header class="masthead" style="background-image: url('{{URL::asset('images/posts/img_' . $image_number . '.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <h1>{{ $topic->title }}</h1>
            <h2 class="subheading">{{ $topic->description }}</h2>
          </div>
        </div>
      </div>
    </div>
  </header>
  
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
  <!-- Main Content -->
  <div class="post_pre">
    <i class="fa fa-align-justify"></i>
    در زیر
    {{ count($texts) }}
    وب‌سایت و خلاصه محتوای آن‌ها درباره
    <b>
    {{ $topic->main_keyword }}
    </b>
    را مشاهده می‌کنید:
  </div>
  @foreach ($texts as $text)
    {!! $text !!}
  @endforeach
        </div>
      </div>
    </div>
  </article>
@stop
