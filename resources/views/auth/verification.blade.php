@extends('layout.default')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>از ثبت نام شما متشکریم. ایمیل فعال سازی برای شما ارسال شد.</h1>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
