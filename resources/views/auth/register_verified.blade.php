@extends('layout.default')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>با تشکر. ثبت نام در سایت تکمیل شد. از حالا می‌توانید از امکانات سایت استفاده کنید.</h1>
            <a href="{{ route('user_dashboard') }}">کنترل پنل</a>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
