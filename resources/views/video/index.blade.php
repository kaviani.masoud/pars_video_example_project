@extends('layout.default')
@section('content')

  <header class="masthead" style="background-image: url('{{URL::asset('images/posts/img_' . 0 . '.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <h1>{{ $video->title }}</h1>
            <h2 class="subheading">
                {{ $video->description }}
            </h2>
          </div>
        </div>
      </div>
    </div>
  </header>
  
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
    <!-- Main Content -->
          <video width="100%" controls>
            <source src="{{ $video->full_path }}" />
          </video>
        </div>
      </div>
      {!! $video->tags_With_routes !!}

    <div class="count_seen">
      <i class="fa fa-eye"></i> {{ $count_seen }}
    </div>
    </div>
@stop
@section('scripts')
<script>
$(document).ready(function(){
  $.get('{{route('user_video_relation', ['action' => 'seen', 'video_id' => $video->id])}}', function() {
  })
})
</script>
@stop
