@extends('layout.default')
@section('content')

  <header class="masthead" style="background-image: url('{{URL::asset('images/posts/img_' . 0 . '.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <h1>PARSVideo | جدیدترین فیلم‌های کاربران</h1>
            <h2 class="subheading">
            بهترین و جدیدترین ویدیوها از سراسر وب ارسال شده توسط کاربران
            </h2>
          </div>
        </div>
      </div>
    </div>
  </header>
  
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
        <div class="home_search">
          <form action="{{ route('search_by_tag') }}">
            <input class="form-control" name="tag" placeholder="جستجو بر اساس تگ"/>
          </form>
        </div>
          <div id="start">
            «
            آخرین ویدیوها
            »
            </div>
    <!-- Main Content -->
    @foreach ($videos as $video)
        <div class="post-preview">
            <a href="{{ route('single_video', ['id' => $video->id, 'url_text_link' => URLHelper::generate_title_for_url($video->title)]) }}">
              <h1 class="post-title">
                <i class="fas fa-graduation-cap"></i> {{$video->title}}
              </h1>
              <h2 class="post-subtitle">
                {{$video->description}}
              </h2>
            </a>
          <hr>
    @endforeach

        </div>
      </div>
    </div>
    {{ $videos->links() }}
@stop
