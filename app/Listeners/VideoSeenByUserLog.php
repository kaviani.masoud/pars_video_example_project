<?php

namespace App\Listeners;

use App\Events\VideoSeenByUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\VideoUserRelation;
use DB;
use Type;

class VideoSeenByUserLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VideoSeenByUser  $event
     * @return void
     */
    public function handle(VideoSeenByUser $event)
    {
        $user_id = $event->user_id;
        $video_id = $event->video_id;
        $IP_address = str_replace(':', '.', $event->IP_address);
        $vur = new VideoUserRelation;
        $vur->video_id = $video_id;
        $vur->user_id = $user_id;
        $vur->created_at = time();
        $vur->type = Type::enumResolve('user_video_relation_types', 2);
        $vur->IP = DB::raw("INET_ATON('$IP_address')");
        $vur->save();
        // To Do: filter based on time
    }
}
