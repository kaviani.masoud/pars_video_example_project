<?php

namespace App\Listeners;

use App\Events\VideoSeenByUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\VideoUserRelation;
use App\Models\VideoUserRelationCache;
use DB;
use Type;

class VideoSeenByUserUpdateCache
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VideoSeenByUser  $event
     * @return void
     */
    public function handle(VideoSeenByUser $event)
    {
        $user_id = $event->user_id;
        $video_id = $event->video_id;
        $IP_address = str_replace(':', '.', $event->IP_address);
        $vur = new VideoUserRelation;
        $vurc = new VideoUserRelationCache;
        $result = $vur::where('video_id', $video_id)
        ->where(function($q) use($IP_address, $user_id){
            $q->where('IP', DB::raw("INET_ATON('$IP_address')"))
            ->orWhere('user_id', $user_id);
        })->first();
        if (!$result)
        {
            $cache_result = $vurc::where('type', Type::enumResolve('user_video_relation_types', 2))->where('video_id', $video_id)->first();
            if (!$cache_result) {
                $new_vurc = new VideoUserRelationCache;
                $new_vurc->type = Type::enumResolve('user_video_relation_types', 2);
                $new_vurc->time_period_in_day = 0;
                $new_vurc->count = 0;
                $new_vurc->video_id = $video_id;
                $new_vurc->save();
            }
            $vurc::where('type', Type::enumResolve('user_video_relation_types', 2))->where('video_id', $video_id)->increment('count', 1);
        }
    }
}
