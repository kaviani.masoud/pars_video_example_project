<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Request;
use App\Models\Video;
use App\Models\VideoUserRelation;
use App;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use Message;
use App\Events\VideoSeenByUser;
use Type;

class RelationController extends BaseController
{
    public function relation(Request $request, $video_id, $action)
    {

        $vur = new VideoUserRelation();
        $user_id = 0;
        if (Auth::user()) {
            $user_id = Auth::user()->id;
        }
        if ($action == Type::enumResolve('user_video_relation_types', 2)) {
            $e = event(new VideoSeenByUser($video_id, $user_id, $request::ip()));
        }
    }

    public function search_by_tag(Request $request)
    {
        $video = new Video;
        $tag = $request::input('tag');
        $videos = $video::whereHas('tags', function($q) use($tag){
            $q->where('name', 'LIKE', '%' . $tag . '%');
        })->orderBy('created_at', 'DESC')->paginate(15);
        $title = 'جستجو برای تگ: ' . $tag;
        $view_params = ['title' => $title, 'videos' => $videos, 'tag' => $tag];
        return view('video.search_by_tag', $view_params);
    }

}
