<?php

namespace App\Http\Controllers\AI;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\Topic;
use App\Models\Post;
use App\Services\Summarizer\Summarizer;

class SummarizeController extends BaseController
{
    public function summarize_one(Request $req)
    {
        $topic = new Topic();
        $post = new Post();
        $summarizer = new Summarizer();
        $latest_topic_in_queue = $topic::where('status', 0)->orderBy('id', 'DESC')->first();
        if (!empty($latest_topic_in_queue)) {
            $posts = $post::where('topics_', $latest_topic_in_queue->id)->get();
            foreach ($posts as $p)
            {
                list($highlights, $keywords, $first_sentence, $summarize_text) = $summarizer->summarize($p->text);
                $update_post = new Post();
                $update_post::where('id', $p->id)
                ->update([
                    'highlights' => $highlights,
                    'keywords' => $keywords,
                    'first_sentence' => $first_sentence,
                    'summarize_text' => $summarize_text,
                    'updated_at' => time()
                ]);
            }
            $topic::where('id', $latest_topic_in_queue->id)
            ->update([
                'status' => 1
            ]);
        }
    }

    public function create_post(Request $req)
    {
        $post = new Post();
        $post->source_link = $req->input('source_link');
        $post->source_name = $req->input('source_name');
        $post->text = $req->input('text');
        $post->topics_ = $req->input('topic_id');
        $post->created_at = time();
        $post->updated_at = time();
        $post->save();
        return redirect()->route('create_post', ['topic_id' => $post->topics_]);
    }
}
