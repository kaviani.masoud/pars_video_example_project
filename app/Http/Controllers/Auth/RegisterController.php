<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Morilog\Jalali\jDateTime;
use Persian;
use Type;
use App\Jobs\SendRegisterVerificationMail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Message;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'user/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        $jalali_date_exploded = explode('/', Persian::Fa2En($data['birthday']));
        $garegorian_date = jDateTime::toGregorian($jalali_date_exploded[0], $jalali_date_exploded[1], $jalali_date_exploded[2]);
        $birthday = strtotime($garegorian_date[2] . '-' . $garegorian_date[1] . '-' . $garegorian_date[0]);
        return \App\Models\User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'birthdate_time' => $birthday,
            'gender' => Type::enumResolve('genders', $data['gender']),
            'reg_token' => md5(rand(0, 1000)),
            'status' => 'inactive'
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        dispatch(new SendRegisterVerificationMail($user));
        return view('auth.verification');
    }

    public function verification($reg_token)
    {
        $user = User::where('reg_token', $reg_token)->first();
        if ($user) {
            $user->reg_token = '';
            $user->status = Type::enumResolve('user_statuses', 3);
            if ($user->save()) {
                Message::produce('اکانت شما با موفقیت فعال شد. لطفا برای استفاده لاگین کنید', 'success');
                return redirect('login');
            }
        } else {
            return redirect('login');
        }
    }
}
