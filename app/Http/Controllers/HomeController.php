<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\Video;
use mersenne_twister\twister;
use DB;
use Type;

class HomeController extends Controller
{
    public function index()
    {
        $video = new Video();
        $videos = $video::where('status', Type::enumResolve('video_statuses', '3'))->orderBy('created_at', 'DESC')->paginate(15);
        $title = 'صفحه اصلی';
        $view_params = ['title' => $title, 'videos' => $videos];
        return view('home/index', $view_params);
    }
}
