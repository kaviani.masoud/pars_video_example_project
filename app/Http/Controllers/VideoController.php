<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Request;
use App\Models\Video;
use App\Models\Tag;
use App;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use Message;
use Type;

class VideoController extends BaseController
{
    public function index(Request $request, $id, $title)
    {
        $video = new Video();
        $v = $video::where('id', $id)->first();
        if (!$v) {
            return abort(404);
        }
        $v->load('count_seen');
        $count_seen_result = $v->count_seen()->first();
        $count_seen = 0;
        if ($count_seen_result) {
            $count_seen = $count_seen_result->count;
        }
        $title = $v->title;
        $view_params = ['title' => $title, 'video' => $v, 'count_seen' => $count_seen];
        return view('video.index', $view_params);
    }

    public function search_by_tag(Request $request)
    {
        $video = new Video;
        $tag = $request::input('tag');
        $videos = $video::where('status', Type::enumResolve('video_statuses', '3'))->whereHas('tags', function($q) use($tag){
            $q->where('name', 'LIKE', '%' . $tag . '%');
        })->orderBy('created_at', 'DESC')->paginate(15);
        $title = 'جستجو برای تگ: ' . $tag;
        $view_params = ['title' => $title, 'videos' => $videos, 'tag' => $tag];
        return view('video.search_by_tag', $view_params);
    }

}
