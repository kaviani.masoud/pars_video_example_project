<?php

namespace App\Http\Controllers\User;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\Post;

class UserController extends BaseController
{
    public function dashboard(Request $req)
    {
        return view('user/dashboard');
    }

    public function create_post(Request $req)
    {
        $post = new Post();
        $post->source_link = $req->input('source_link');
        $post->source_name = $req->input('source_name');
        $post->text = $req->input('text');
        $post->topics_ = $req->input('topic_id');
        $post->save();
        return redirect()->route('create_post', ['topic_id' => $post->topics_]);
    }
}
