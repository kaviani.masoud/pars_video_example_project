<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\Topic;

class TopicController extends BaseController
{
    public function create(Request $req)
    {
        return view('admin/topic/create');
    }

    public function create_post(Request $req)
    {
        $topic = new Topic;
        $topic->title = $req->input('title');
        $topic->description = $req->input('description');
        $topic->url_text_link = $req->input('url_text_link');
        $topic->main_keyword = $req->input('main_keyword');
        $topic->created_at = time();
        $topic->updated_at = time();
        $topic->status = 0;
        $topic->save();
        return redirect()->route('create_post', ['topic_id' => $topic->id]);
    }
}
