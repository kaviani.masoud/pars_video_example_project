<?php

namespace App\Http\Controllers\User;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Request;
use App\Models\Video;
use App;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use App\Models\Tag;
use Message;
use Masih\YoutubeDownloader\YoutubeDownloader;
use App\Jobs\YoutubeDownload;
use Type;

class VideoController extends BaseController
{
    public function create(Request $request)
    {
        return view('user.video.create');
    }

    public function create_post(Request $request)
    {
        $uploaded_file_name = App::make('UploadService')->upload_video($request::file('video'));
        DB::beginTransaction();
        $video = new Video();
        $video->title = $request::input('title');
        $video->description = $request::input('description');
        $video->machine_name = $uploaded_file_name;
        $video->user_id = Auth::user()->id;
        $video->save();
        $tag = new Tag();
        $tag_ids = $tag->insert_batch_getIDs(explode('-', $request::input('tags')));
        $video->tags()->sync($tag_ids);
        DB::commit();
        Message::produce('ویدیو با موفقیت اضافه شد', 'success');
        return redirect()->route('user_video_list');
    }

    public function get_list(Request $request)
    {
        $video = new Video();
        $videos = $video::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(15);
        $title = 'لیست ویدیوها';
        $view_params = ['title' => $title, 'videos' => $videos];
        return view('user.video.get_list', $view_params);
    }

    public function delete(Request $request, $id)
    {
        $video = new Video();
        $videos = $video::where('id', $id)->delete();
        Message::produce('ویدیو با موفقیت حذف شد', 'success');
        return redirect()->route('user_video_list');
    }

    public function youtube(Request $request)
    {
        return view('user.video.youtube');
    }

    public function youtube_download(Request $request)
    {
        DB::beginTransaction();
        $video = new Video();
        $video->title = $request::input('title');
        $video->description = $request::input('description');
        $video->machine_name = App::make('UploadService')->generate_random_filename() . '.mp4';
        $video->user_id = Auth::user()->id;
        $video->status = Type::enumResolve('video_statuses', 2);
        $video->save();
        $tag = new Tag();
        $tag_ids = $tag->insert_batch_getIDs(explode('-', $request::input('tags')));
        $video->tags()->sync($tag_ids);
        DB::commit();
        $parsed_url_query = parse_url($request::input('address'))['query'];
        $query = null;
        $parsed_string = parse_str($parsed_url_query, $query);
        $v = $query['v'];
        $ytb = (new YoutubeDownload($v, $video))->delay(now()->addSeconds(1));
        dispatch($ytb);
        Message::produce('ویدیو با موفقیت در صف دانلود قرار گرفت.', 'success');
        return redirect()->route('user_video_list');
    }
}
