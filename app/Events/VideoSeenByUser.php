<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class VideoSeenByUser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $video_id;
    public $user_id;
    public $IP_address;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($video_id, $user_id, $IP_address)
    {
        $this->video_id = $video_id;
        $this->user_id = $user_id;
        $this->IP_address = $IP_address;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
