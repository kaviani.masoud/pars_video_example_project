<?php

namespace App\Services\Upload;

use URL;

class Upload
{
    private $folder = '';
    private $path = '';

    public function __construct()
    {
        $this->folder = 'uploads';
        $this->path = public_path() . '/' . $this->folder;
    }

    public function generate_random_filename()
    {
        $filename = substr(md5(rand(1, 999999999)), 0, 10) . '-' . time();
        // ToDo: must check if file exists, althou it's very rare
        return $filename;
    }

    public function get_file_path($file, $absolute_path=false)
    {
        if ($absolute_path) {
            return $this->path;  
        }
        return URL::asset($this->folder . '/' . $file);
    }

    public function upload_video($file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = $this->generate_random_filename() . '.' . $extension;
        $moved_file = $file->move($this->path, $filename);
        return $filename;
    }
}
