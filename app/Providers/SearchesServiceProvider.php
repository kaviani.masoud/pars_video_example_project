<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Search\Search;

class SearchesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SearchService', function ($app) {
            return new Search;
        });
    }
}
