<?php

namespace App\Providers;

use App\Helpers\PersiansFacades;
use App;
use Illuminate\Support\ServiceProvider;

class PersiansFacadesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('persians', function () {
            return new PersiansFacades;
        });
    }
}
