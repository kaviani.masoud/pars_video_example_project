<?php

namespace App\Providers;

use App\Helpers\URLHelperFacades;
use Illuminate\Support\ServiceProvider;

class URLHelperFacadesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('urlhelper', function ($app) {
            return new URLHelperFacades;
        });
    }
}
