<?php

namespace App\Providers;

use App\Helpers\MessagesFacades;
use App;
use Illuminate\Support\ServiceProvider;

class MessagesFacadesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('messages', function () {
            return new MessagesFacades;
        });
    }
}
