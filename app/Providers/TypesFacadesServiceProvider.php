<?php

namespace App\Providers;

use App\Helpers\TypesFacades;
use App;
use Illuminate\Support\ServiceProvider;

class TypesFacadesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('types', function () {
            return new TypesFacades;
        });
    }
}
