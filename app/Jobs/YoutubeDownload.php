<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Type;
use Masih\YoutubeDownloader\YoutubeDownloader;

class YoutubeDownload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $v = '';
    protected $video = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($v, $video)
    {
        $this->v = $v;
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $youtube = new YoutubeDownloader($this->v);
        // $youtube->setPath(public_path() . '/uploads');
        // $result = $youtube->download();
        // rename($result, public_path() . '/uploads/' . $this->video->machine_name);
        $this->video->status = Type::enumResolve('video_statuses', 3);
        $this->video->save();
    }
}
