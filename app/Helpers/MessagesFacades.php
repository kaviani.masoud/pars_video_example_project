<?php
namespace App\Helpers;

class MessagesFacades
{
    public function produce($string, $status_code)
    {
        request()->session()->flash('message', $string);
        request()->session()->flash('class', $status_code);
        return true;
    }

    public function consume()
    {
        $message = request()->session()->pull('message');
        $class = request()->session()->pull('class', 'sucess');
        if ($message) {
            return ['message' => $message, 'class' => $class];
        }
        return false;
    }
}