<?php
namespace App\Helpers;

class TypesFacades
{

    private $types = [];
    private $enums = [];

    public function __construct()
    {
        $this->types = [
            'genders' => [
                2 => 'مرد',
                3 => 'زن'
            ]
        ];
        $this->enums = [
            'genders' => [
                2 => 'male',
                3 => 'female'
            ],
            'user_statuses' => [
                2 => 'inactive',
                3 => 'active',
                5 => 'pending'
            ],
            'video_statuses' => [
                2 => 'inactive',
                3 => 'active'
            ],
            'user_video_relation_types' => [
                2 => 'seen',
                3 => 'like',
                5 => 'bookmark',
                7 => 'full_seen'
            ]
        ];
        $this->human_readables = [
            'video_statuses' => [
                'inactive' => 'غیرفعال',
                'active' => 'فعال'
            ]
        ];
    }

    public function humanReadablesResolve($type ,$key)
    {
        return $this->human_readables[$type][$key];
    }
    
    public function enumResolve($type ,$key)
    {
        return $this->enums[$type][$key];
    }

    public function generate_select_box($type, $selected_id, $name, $blank=false, $class='form-control')
    {
        if (array_key_exists($type, $this->types)) {
            $types_array = $this->types[$type];
            $dropdown_str = "<select name='$name' class='$class'>";
            if ($blank) {
                $dropdown_str .= "<option value='-1'>$blank</option>";
            }
            foreach ($types_array as $key => $val) {
                $selected_str = '';
                if ($key == $selected_id) {
                    $selected_str = 'selected=selected';
                }
                $dropdown_str .= "<option $selected_str value='$key'>$val</option>";
            }
            $dropdown_str .= '</select>';
            return $dropdown_str;
        } else {
            throw new \Exception('There is not such type');
        }
    }
}