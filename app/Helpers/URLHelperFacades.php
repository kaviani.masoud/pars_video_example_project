<?php
namespace App\Helpers;

class URLHelperFacades
{
    public function generate_title_for_url($title)
    {
        return str_replace(' ', '_', $title);
    }
}