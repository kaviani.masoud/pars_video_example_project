<?php
namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class PersiansFacades extends Facade
{
    protected static function getFacadeAccessor() { return 'persians'; }
}