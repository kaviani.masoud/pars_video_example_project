<?php
namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class TypesFacades extends Facade
{
    protected static function getFacadeAccessor() { return 'types'; }
}