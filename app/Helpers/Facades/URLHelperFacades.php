<?php
namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class URLHelperFacades extends Facade
{
    protected static function getFacadeAccessor() { return 'urlhelper'; }
}