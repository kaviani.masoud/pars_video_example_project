<?php
namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class MessagesFacades extends Facade
{
    protected static function getFacadeAccessor() { return 'messages'; }
}