<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\Upload\Upload;

class VideoUserRelation extends Model
{
    protected $table = 'videos_users_relations';
    public $timestamps = false;
    protected $fillable = [
        'video_id', 'user_id', 'created_at', 'type'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function video()
    {
        return $this->belongsTo('App\Models\Video');
    }
    
    public function getDateFormat()
    {
        return 'U';
    }

}
