<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\Upload\Upload;
use App\Models\VideoUserRelationCache;
use Type;

class Video extends Model
{
    protected $table = 'videos';

    use SoftDeletes;

    protected $fillable = [
        'title', 'description', 'machine_name', 'user_id', 'length'
    ];

    public function rules(Request $request)
    {
        return [
            'title' => 'required|min:25'
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function count_seen()
    {
        return $this->hasOne('App\Models\VideoUserRelationCache')->where('type', Type::enumResolve('user_video_relation_types', 2));
    }
    
    public function getDateFormat()
    {
        return 'U';
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'videos_tags', 'tag_id', 'video_id');
    }

    public function getFullPathAttribute()
    {
        $upload = new Upload();
        $full_path = $upload->get_file_path($this->machine_name);
        return $full_path;
    }

    public function getTagsWithRoutesAttribute()
    {
        $tags = $this->tags;
        $tags_with_url = [];
        foreach ($tags as $tag)
        {
            $tags_with_url[] = '<a href="' . route('search_by_tag', ['tag' => $tag->name]) . '">' . $tag->name . '</a>';
        }
        return implode('، ', $tags_with_url);
    }
}
