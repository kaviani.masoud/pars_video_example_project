<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\Upload\Upload;

class VideoUserRelationCache extends Model
{
    protected $table = 'videos_users_relations_cache';

    protected $fillable = [
        'type', 'time_period_in_day', 'count'
    ];
    
    public function getDateFormat()
    {
        return 'U';
    }

}
