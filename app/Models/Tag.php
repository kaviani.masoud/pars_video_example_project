<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class Tag extends Model
{
    protected $table = 'tags';
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];
    
    public function videos()
    {
        return $this->belongsToMany('App\Models\Video', 'videos_tags', 'tag_id', 'video_id');
    }

    public function insert_batch_getIDs(array $tags)
    {
        $return_tags = [];
        $time = time();
        foreach ($tags as $tag)
        {
            $t = $this->where([['name', $tag]])->first();
            if ($t) {
                $return_tags[$t->id] = ['created_at' => $time];
            } else {
                $tm = new Tag();
                $tm->name = $tag;
                $tm->save();
                $return_tags[$tm->id] = ['created_at' => $time];
            }
        }
        return $return_tags;
    }

    public function getDateFormat()
    {
        return 'U';
    }
}
