<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('User')->group(function(){
    Route::group(['prefix' => 'user', 'middleware' => 'auth'], function(){
        Route::get('dashboard', 'UserController@dashboard')->name('user_dashboard');
        Route::prefix('video')->group(function(){
            Route::get('create', 'VideoController@create')->name('user_upload_video');
            Route::get('youtube', 'VideoController@youtube')->name('user_youtube');
            Route::post('youtube', 'VideoController@youtube_download')->name('user_youtube_download');
            Route::post('create', [
                'uses' => 'VideoController@create_post',
                'middleware' => 'validator:App\Models\Video'
            ])->name('user_upload_video');
            Route::get('list', 'VideoController@get_list')->name('user_video_list');
            Route::get('edit/{id}', 'VideoController@edit')->name('user_video_edit');
            Route::get('delete/{id}', 'VideoController@delete')->name('user_video_delete');
        });
    });
});

Route::get('v/{id}/{url_text_link}', 'VideoController@index')->name('single_video');
Route::get('random', 'VideoController@random')->name('random_video');
Route::get('s', 'VideoController@search_by_tag')->name('search_by_tag');
Route::get('relation/{video_id}/{action}', 'RelationController@relation')->name('user_video_relation');

Route::prefix('pages')->group(function(){
        Route::get('/about_us', 'PageController@about_us')->name('about_us');
        Route::get('/contac_us', 'PageController@contact_us')->name('contact_us');
});

Auth::routes();
Route::namespace('Auth')->group(function(){
    Route::get('/regisger_verification/{token}', 'RegisterController@verification');
});

Route::get('/', 'HomeController@index');
